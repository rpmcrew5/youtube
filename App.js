/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './component/vedioItem';
import data from './data/data.json';

const App = () => {
  return (
    <View style={Setting.container}>
      <View style={Setting.navBar}>
        <Image
          source={require('./assets/logo.png')}
          style={{width: 98, height: 22}}
        />
        <View style={Setting.rightNav}>
          <TouchableOpacity>
            <Icon style={Setting.navItem} name="search" size={25} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Icon style={Setting.navItem} name="account-circle" size={25} />
          </TouchableOpacity>
        </View>
      </View>
      <View style={Setting.body}>
        <FlatList
          data={data.items}
          renderItem={video => <VideoItem video={video.item} />}
          keyExtractor={item => item.id}
          ItemSeparatorComponent={() => (
            <View style={{height: 0.5, backgroundColor: '#E5E5E5'}} />
          )}
        />
      </View>
    </View>
  );
};

const Setting = StyleSheet.create({
  container: {
    flex: 1,
  },
  img: {
    width: 120,
    height: 30,
    marginTop: 5,
  },
  kotak: {
    backgroundColor: 'white',
  },
  rightNav: {
    flexDirection: 'row',
  },
  navItem: {
    marginLeft: 25,
  },
  navBar: {
    height: 55,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  body: {
    flex: 1,
  },
});

export default App;
